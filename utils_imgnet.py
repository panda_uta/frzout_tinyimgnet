#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import sys
import time
import json
import logging
import path
import math
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable as V
import torchvision.datasets as dset
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
import random

import torchvision.datasets as datasets
from torch.utils.data.sampler import SubsetRandomSampler


batch_sz = 0
train_sz = 0

def get_train_batch_ratio():
    # print("get_train_batch_ratio train_sz :", train_sz)
    # print("get_train_batch_ratio batch_sz :", batch_sz)
    return train_sz/batch_sz 

random.seed(0)
scale_fn = {'linear':lambda x: x,
                         'squared': lambda x: x**2,
                         'cubic': lambda x: x**3}
                         
def calc_speedup(growthRate,nDenseBlocks,t_0,how_scale):
    # Height*Width at each stage
    HW = [32**2, 16**2, 8**2]

    # FLOPs of first layer
    c = [3* (2*growthRate)*HW[0]*9]
    # num channels
    n = 2
    for i in range(3):
        for j in range(nDenseBlocks):
            # Calc flops for this layer
            c.append(n*(4*growthRate*growthRate)*HW[i] + 4*9*growthRate*growthRate*HW[i])
            n +=1
        n = math.floor(n*0.5)
    
    # Total computational cost for training run without freezeout
    C = 2*sum(c)

    # Computational Cost with FreezeOut
    C_f = sum(c)+sum([c_i*scale_fn[how_scale](
                    (t_0 + (1 - t_0) * float(index) / len(c) ))
                    for index,c_i in enumerate(c)])

    
    if how_scale=='linear':
        return 1.3*(1-float(C_f)/C)
    else:
        return 1-float(C_f)/C

		
def get_data_loader_imgnet(which_dataset,augment=True,validate=True,batch_size=50):
    		
    class ImageFolder(dset.ImageFolder):
        def __len__(self):
            if self.train:
                return len(self.train_data)
            else:
                return 10000
	
    global train_sz
    global batch_sz			
	
    print('Loading IMAGENET 12...')
    norm_mean = [0.485, 0.456, 0.406]
    norm_std = [0.229, 0.224, 0.225]

    dataset = ImageFolder
	
	    # Prepare transforms and data augmentation

    norm_transform = transforms.Normalize(norm_mean, norm_std)
	
	    
	
    print("Imagenet 12, getting the images from the directory...")
    traindir = os.path.join('./ImageFolder/tiny-imagenet-200', 'train')
    valdir = os.path.join('./ImageFolder/tiny-imagenet-200', 'val')
	
	
    # train_set = datasets.ImageFolder(
    # traindir,
    # transforms.Compose([
        # transforms.RandomResizedCrop(224),
        # transforms.RandomHorizontalFlip(),
        # transforms.ToTensor(),
        # norm_transform
    # ]))
	

    train_set = datasets.ImageFolder(
    traindir,
    transforms.Compose([
        transforms.RandomCrop(64, padding=4),
#        transforms.RandomCrop(28, padding=4),

        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        norm_transform
    ]))
	
    test_set = datasets.ImageFolder(valdir, transforms.Compose([
            # transforms.RandomCrop(64, padding=4),
            # transforms.RandomHorizontalFlip(),			
            transforms.ToTensor(),
            norm_transform
    ]))
	
# validation_data = datasets.ImageFolder(validation_root,
                                               # transform=transforms.Compose([transforms.Scale(256),
                                                                             # transforms.CenterCrop(224),
                                                                             # transforms.ToTensor(),
                                                                             # normalize]))	
	
    # test_set = datasets.ImageFolder(valdir, transforms.Compose([
            # transforms.Scale(256),
            # transforms.CenterCrop(224),		
            # transforms.ToTensor(),
            # norm_transform
    # ]))	

    # print("printing the attributes of test_set ...")
#    print(test_set.__dict__)	

    print("len(test_set) :", len(test_set))
    print("len(train_set) :", len(train_set))
    kwargs = {'num_workers': 1, 'pin_memory': False}
	
    # train_set.train_data = train_set.train_data[48000:50000]
    # train_set.train_labels = train_set.train_labels[48000:50000]
	
	
    # test_set.train_data = test_set.test_data
    # test_set.train_labels = test_set.test_labels

    valid_size = 0.9
    num_train = len(train_set)
    indices = list(range(num_train))
    split = int(np.floor(valid_size * num_train))


    np.random.seed(0)
    np.random.shuffle(indices)

    train_idx, valid_idx = indices[split:], indices[:split]
	
    print("len(train_idx) :", len(train_idx))
    print("len(valid_idx) :", len(valid_idx))	
	
    train_sampler = SubsetRandomSampler(train_idx)
    valid_sampler = SubsetRandomSampler(valid_idx)
   

    # Prepare data loaders
    train_loader = DataLoader(train_set, batch_size=batch_size, 
                              shuffle=True, **kwargs)
    # train_loader = DataLoader(train_set, batch_size=batch_size, sampler=train_sampler,
                               # **kwargs)							  
    test_loader = DataLoader(test_set, batch_size=batch_size,
                             shuffle=False, **kwargs)
    
   

    # print("train_set size = ", len(train_set.train_data))
    # print("test_set size = ", len(test_set.train_data))

#Set the train_sz and batch_sz to be used in densenet

    # train_sz = len(train_set.train_labels)
    train_sz = len(train_set)	

    batch_sz = batch_size
    print("Utils train_sz :", train_sz)
    print("Utils batch_sz :", batch_sz)
    print("Returning for Imagenet 12 ...")
		
    # return train_loader, test_loader, len(train_set.train_data), len(test_set.train_data)
    return train_loader, test_loader
	

    

class MetricsLogger(object):

    def __init__(self, fname, reinitialize=False):
        self.fname = path.Path(fname)
        self.reinitialize = reinitialize
        if self.fname.exists():
            if self.reinitialize:
                logging.warn('{} exists, deleting'.format(self.fname))
                self.fname.remove()

    def log(self, record=None, **kwargs):
        """
        Assumption: no newlines in the input.
        """
        if record is None:
            record = {}
        record.update(kwargs)
        record['_stamp'] = time.time()
        with open(self.fname, 'a') as f:
            f.write(json.dumps(record, ensure_ascii=True)+'\n')


def read_records(fname):
    """ convenience for reading back. """
    skipped = 0
    with open(fname, 'rb') as f:
        for line in f:
            if not line.endswith('\n'):
                skipped += 1
                continue
            yield json.loads(line.strip())
        if skipped > 0:
            logging.warn('skipped {} lines'.format(skipped))
            
"""
Very basic progress indicator to wrap an iterable in.

Author: Jan Schlüter
"""


def progress(items, desc='', total=None, min_delay=0.1):
    """
    Returns a generator over `items`, printing the number and percentage of
    items processed and the estimated remaining processing time before yielding
    the next item. `total` gives the total number of items (required if `items`
    has no length), and `min_delay` gives the minimum time in seconds between
    subsequent prints. `desc` gives an optional prefix text (end with a space).
    """
    total = total or len(items)
    t_start = time.time()
    t_last = 0
    for n, item in enumerate(items):
        t_now = time.time()
        if t_now - t_last > min_delay:
            print("\r%s%d/%d (%6.2f%%)" % (
                    desc, n+1, total, n / float(total) * 100), end=" ")
            if n > 0:
                t_done = t_now - t_start
                t_total = t_done / n * total
                print("(ETA: %d:%02d)" % divmod(t_total - t_done, 60), end=" ")
            sys.stdout.flush()
            t_last = t_now
        yield item
    t_total = time.time() - t_start
    print("\r%s%d/%d (100.00%%) (took %d:%02d)" % ((desc, total, total) +
                                                   divmod(t_total, 60)))
